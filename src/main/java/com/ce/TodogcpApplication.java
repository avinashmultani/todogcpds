package com.ce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodogcpApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodogcpApplication.class, args);
	}

}
